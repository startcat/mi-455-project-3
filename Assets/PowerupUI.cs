using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupUI : MonoBehaviour
{

    public List<GameObject> powerupUIComponents;

    public Speed speed;

    public GameObject speedPowerupUI;

    // Start is called before the first frame update
    void Start()
    {
        //for(int i = 0; i < transform.childCount; i++)
        //{
        //    powerupUIComponents.Add(transform.GetChild(i));
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if(speed.active == true)
        {
            speedPowerupUI.SetActive(true);
        }
        else
        {
            speedPowerupUI.SetActive(true);
        }
    }
}
