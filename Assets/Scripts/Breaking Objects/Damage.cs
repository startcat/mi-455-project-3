using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public float damageMultiplier;

    private Roll roll;

    private ObjectHealth objectHealth;

    private Breaking breaking;

    private Vector3 velocity;

    public float damageDone;




    private void OnCollisionEnter(Collision col)
    {
        //if the player collides with a breaking object
        if (col.gameObject.CompareTag("Breaking"))
        {
            //access the roll script
            roll = this.gameObject.GetComponent<Roll>();
            //access the object health script
            objectHealth = col.gameObject.transform.parent.gameObject.GetComponent<ObjectHealth>();
            //objectHealth = col.gameObject.GetComponent<ObjectHealth>();

            //get the velocity value from the roll script
            velocity = roll.myBod.velocity;

            //damage = the strength of the velocty vector3 * a multiplier for damage
            damageDone = velocity.magnitude * damageMultiplier;
            //Debug.Log(damageDone);

            //breaking = col.gameObject.GetComponent<Breaking>();
            //breaking.Damage(damageDone);

            //call Damage() from object health
            objectHealth.Damage(damageDone);
        }
        else if (col.gameObject.CompareTag("Stay"))
        {
            objectHealth = col.gameObject.GetComponent<ObjectHealth>();
            roll = gameObject.GetComponent<Roll>();
            velocity = roll.myBod.velocity;
            damageDone = velocity.magnitude * damageMultiplier;
            objectHealth.Damage(damageDone);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        //if the player collides with a breaking object
        if (col.gameObject.CompareTag("Breaking"))
        {
            //access the roll script
            roll = this.gameObject.GetComponent<Roll>();
            //access the object health script
            //objectHealth = col.gameObject.transform.parent.gameObject.GetComponent<ObjectHealth>();
            //objectHealth = col.gameObject.GetComponent<ObjectHealth>();

            //get the velocity value from the roll script
            velocity = roll.myBod.velocity;

            //damage = the strength of the velocty vector3 * a multiplier for damage
            damageDone = velocity.magnitude * damageMultiplier;
            //Debug.Log(damageDone);

            breaking = col.gameObject.GetComponent<Breaking>();
            breaking.Damage(damageDone);

            //call Damage() from object health
            //objectHealth.Damage(damageDone);
        }
        else if (col.gameObject.CompareTag("Stay"))
        {
            objectHealth = col.gameObject.GetComponent<ObjectHealth>();
            roll = gameObject.GetComponent<Roll>();
            velocity = roll.myBod.velocity;
            damageDone = velocity.magnitude * damageMultiplier;
            objectHealth.Damage(damageDone);
        }
    }

}
