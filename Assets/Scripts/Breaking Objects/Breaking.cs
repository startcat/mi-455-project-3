using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaking : MonoBehaviour
{
    public List<GameObject> states;
    private int stateIndex = 0;

    public float health;

    private Roll roll;

    private Vector3 velocity;

    private Transform oldTransform;

    private GameObject parentOfThis;

    //private float damage;

    public float damageMultiplier;

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        Debug.Log("player collision");
    //        //calculate the damage done
    //        roll = collision.gameObject.GetComponent<Roll>();
    //        velocity = roll.myBod.velocity;
    //        damage = velocity.magnitude * damageMultiplier;

    //        //update health
    //        health -= damage;

    //        //move thru dif states based on object health
    //        if(health <= health / states.Count)
    //        {
    //            //move state index to next pos
    //            stateIndex += 1;
    //            //destroy all children of the object
    //            foreach(Transform child in this.transform)
    //            {
    //                GameObject.Destroy(child.gameObject);
    //            }
    //            //instantiate the new state as a child of the object
    //            Instantiate(states[stateIndex], this.transform.position, this.transform.rotation, this.transform);
    //        }

    //    }
    //}


    public void Damage(float damage)
    {
        //Debug.Log("BREAKIGN DAMAGE");
        health -= damage;
        //Debug.Log(states.Count);
        //move thru dif states based on object health
        if (health < 5 && health > 1)
        {
            Debug.Log("HEALTH BELOW HALF");
            //move state index to next pos
            stateIndex += 1;
            //destroy all children of the object
            foreach (Transform child in this.transform)
            {
                oldTransform = child.transform;
                GameObject.Destroy(child.gameObject);
            }
            //instantiate the new state as a child of the object
            Instantiate(states[stateIndex], oldTransform.position, oldTransform.rotation, this.transform);
        }

    if(health <= 0)
        {
            parentOfThis = this.gameObject.transform.parent.gameObject;

            stateIndex += 1;
            //destroy all children of the object
            GameObject.Destroy(this.gameObject);
            //instantiate the new state as a child of the object
            Instantiate(states[stateIndex], oldTransform.position, oldTransform.rotation, this.transform);
        }
    }

}
