using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BadBreak : MonoBehaviour
{
    [SerializeField]
    private GameObject mainCamera;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ImageGradient.badBreak = true;
            mainCamera.GetComponent<MainCamera>().GameOverBreak(this.transform);
        }
    }
}
