using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FracturePoints : MonoBehaviour
{
    [Range(1, 100)] public int breakScore;

    // Update is called once per frame
    void OnDisable()
    {
        if (ImageGradient.badBreak == false && Timer.time > 0.1)
        {
            GM.instantPoints(breakScore * 100);
        }
    }
}
