using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowForward : MonoBehaviour
{
    public Transform follow;

    private float lastRotation;
    public float targetRotation;
    public float currentRotation;

    public float turnTime = 0.2f;
    private float currentTurnTime;

    private Roll myRoller;

    public GameObject sprintTrails;

    // Start is called before the first frame update
    void Start()
    {
        currentTurnTime = turnTime;
        if (follow) { myRoller = follow.GetComponent<Roll>(); }
    }

    // Update is called once per frame
    void Update()
    {
        if (follow)
        {
            transform.position = follow.position;
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                targetRotation = 0f; // Full Right
                if (Input.GetAxis("Vertical") > 0f) 
                { 
                    targetRotation = 270f; // All Up
                    if (Input.GetAxis("Horizontal") > 0f) { targetRotation += 45f; }
                    if (Input.GetAxis("Horizontal") < 0f) { targetRotation -= 45f; }
                }
                else if (Input.GetAxis("Vertical") < 0f) 
                {
                    targetRotation = 90f; // All Down
                    if (Input.GetAxis("Horizontal") > 0f) { targetRotation -= 45f; } 
                    if (Input.GetAxis("Horizontal") < 0f) { targetRotation += 45f; }
                }
                else if (Input.GetAxis("Horizontal") < 0f) { targetRotation = 180f; }  // Full Left


                if (targetRotation != lastRotation) //Fix so turn shortest direction, not always clockwise
                { 
                    lastRotation = currentRotation; currentTurnTime = 0f;
                    if (Mathf.Abs(currentRotation - targetRotation) > 180f) 
                    { 
                        if(currentRotation > targetRotation) { targetRotation += 360f; }
                        else { targetRotation -= 360f; }
                    }               
                }
            }

            if (currentTurnTime <= turnTime) { currentTurnTime += Time.deltaTime; }
            currentRotation = Mathf.Lerp(lastRotation, targetRotation, currentTurnTime / turnTime);
            transform.rotation = Quaternion.Euler(0, currentRotation, 0);

            if (myRoller && sprintTrails)
            {
                if (myRoller.sprinting) { sprintTrails.SetActive(true); }
                else { sprintTrails.SetActive(false); }
            }

        }
    }
}
