using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glider : MonoBehaviour
{
    [Range(0f, 20f)] public float activeTime = 10f;

    [Range(0f, -20f)] public float glideFallSpeed = -1f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(GliderActive(other));
            gameObject.transform.position = new Vector3(0, -10000, 0);

        }
    }

    public IEnumerator GliderActive(Collider collision)
    {
        var roller = collision.gameObject.GetComponent<Roll>();

        roller.glideFallSpeed = glideFallSpeed;
        roller.gliding = true;

        yield return new WaitForSeconds(activeTime);
        
        roller.gliding = false;
        Destroy(gameObject);
    }
}
