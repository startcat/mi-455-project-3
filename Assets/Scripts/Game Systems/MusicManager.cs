using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [Header("FMOD Music Event")]
    [SerializeField]
    FMODUnity.EventReference mainMusic;
    private FMOD.Studio.EventInstance musicInstance;

    [Header("Chaos Level Thresholds")]
    [SerializeField]
    private float levelTwoThresh;
    [SerializeField]
    private float levelThreeThresh;
    [SerializeField]
    private float levelFourThresh;
    [SerializeField]
    private float levelFiveThresh;
    [SerializeField]
    private float levelSixThresh;

    [Header("Debug")]
    [SerializeField]
    private float currentChaosAmt = 0;
    private float lastChaosAmt = 0;
    private string chaosParam = "Chaos Level";
    public GM gameManager;

    // Start is called before the first frame update
    void Start()
    {
        musicInstance = FMODUnity.RuntimeManager.CreateInstance(mainMusic);
        musicInstance.setParameterByName(chaosParam, 1);
        musicInstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        // TODO Calcuate Chaos Amt to determine # we'll compare stuff with
        currentChaosAmt = GM.score + GM.combo*2 + GM.currentComboPoints;

        // Pass if nothin' happened
        if (lastChaosAmt == currentChaosAmt) return;

        // Set chaos level in FMOD
        if (currentChaosAmt < levelTwoThresh)
        {
            musicInstance.setParameterByName(chaosParam, 1);
        }

        else if (currentChaosAmt < levelThreeThresh)
        {
            musicInstance.setParameterByName(chaosParam, 2);
        }

        else if (currentChaosAmt < levelFourThresh)
        {
            musicInstance.setParameterByName(chaosParam, 3);
        }

        else if (currentChaosAmt < levelFiveThresh)
        {
            musicInstance.setParameterByName(chaosParam, 4);
        }

        else if (currentChaosAmt < levelSixThresh)
        {
            musicInstance.setParameterByName(chaosParam, 5);
        }

        else if (currentChaosAmt >= levelSixThresh)
        {
            musicInstance.setParameterByName(chaosParam, 6);
        }
    }

    void OnGUI()
    {
        //GUILayout.Box(string.Format("Score: {0}, Combo: {1}, NewScore: {2}, CurrentScore: {3}", GM.score, GM.combo, newScore, currentScore));
    }
}
