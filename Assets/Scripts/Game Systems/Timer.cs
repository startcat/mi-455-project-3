using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timerText;
    [SerializeField]
    private float remainingTime;
    [SerializeField]
    private GM gm;
    public static float time;

    // Update is called once per frame
    void Update()
    {
        remainingTime -= Time.deltaTime;
        time = remainingTime;
        if (remainingTime <= 0.1)
        {
            SceneManager.LoadScene("GameOver");
        }
        int minutes = Mathf.FloorToInt(remainingTime / 60);
        int seconds = Mathf.FloorToInt(remainingTime % 60);
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
