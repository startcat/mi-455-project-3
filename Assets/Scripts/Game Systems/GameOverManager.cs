using FMOD;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    private int points = GM.score + ((int)GM.currentComboPoints);
    private int pointsToWin = ImageGradient.total;
    [SerializeField]
    private TextMeshProUGUI winLoseText;
    [SerializeField]
    private TextMeshProUGUI pointsText;
    // Start is called before the first frame update
    void Start()
    {
        if (points >= pointsToWin)
        {
            winLoseText.text = "YOU WIN!";
        }
        else
        {
            winLoseText.text = "You ran out of time, Better Luck Next Time";
        }
        if (ImageGradient.badBreak)
        {
            winLoseText.text = "You hit an important relic, Better Luck Next Time";
        }
        pointsText.text = "You costed the company $" + points.ToString() + " the amount to beat was $" + ImageGradient.total.ToString();
    }

    public void ToMainMenu()
    {
        GM.score = 0;
        SceneManager.LoadScene("MainMenu");
    }
    
}
