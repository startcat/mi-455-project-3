using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        //To have the dithering work well with reusing materials we need to instance every material
        //If we want to change a material and need it to not be instanced I can optimize this in the future, Adam is currently very sleepy
        List<GameObject> allObj = GetAllObjectsInScene();
        foreach(GameObject GO in allObj)
        {
            if (GO.GetComponent<Renderer>() != null)
            {
                Material mat = GO.GetComponent<Renderer>().material;
                GO.GetComponent<Renderer>().material = new Material(mat);
            }
            
        }
    }
    /// <summary>
    /// Gets all objects in the scene
    /// </summary>
    /// <returns>A list of all gameobjects</returns>
    List<GameObject> GetAllObjectsInScene()
    {
        List<GameObject> objectsInScene = new List<GameObject>();

        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave)
                continue;


            objectsInScene.Add(go);
        }

        return objectsInScene;
    }
}
