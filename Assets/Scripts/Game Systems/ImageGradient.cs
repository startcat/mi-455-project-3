using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageGradient : MonoBehaviour
{
    [SerializeField] private Gradient _gradient = null;
    [SerializeField] private Image _image = null;
    [SerializeField] private Slider slider = null;
    public static int total;
    public static bool badBreak;
    [SerializeField]
    private int selfTotal;
    
    private void Awake()
    {
        badBreak = false;
        total = selfTotal;
        slider.maxValue = total;
        _image = GetComponent<Image>();
    }

    private void Update()
    {
        float fillAmount = (total - (GM.score + GM.currentComboPoints)) / total;
        if (fillAmount > 1)
        {
            fillAmount = 1;
        }
        _image.color = _gradient.Evaluate(fillAmount);
    }
}
