using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dithering : MonoBehaviour
{
    private Material mat;

    [Header("Check this is you have a basic color material")]
    public bool generateBasicMaterial = false;
    // Start is called before the first frame update
    void Start()
    {
        mat = gameObject.GetComponent<Renderer>().material;
        if (generateBasicMaterial)
        {
            Shader shader = Resources.Load<Shader>("DitherBasic");
            Color color = mat.color;
            mat = new Material(shader);
            mat.color = color;
            gameObject.GetComponent<Renderer>().material = mat;
        }

    }

    /// <summary>
    /// Changes the Dithering based on how close the object is to the camera
    /// The farther the object (and closer to the player) the stronger the dithering
    /// </summary>
    /// <param name="distance"> distance to object</param>
    /// <param name="maxDistance">distance to player</param>
    public void SetOpacity(float distance, float distanceToPlayer)
    {
        float opacity = 0;
        opacity = ((distanceToPlayer - distance)/distanceToPlayer);
        opacity = 1 - opacity;
        mat.SetFloat("_Opacity", opacity);
    }

    /// <summary>
    /// Just resets the opacity
    /// </summary>
    /// <param name="f">Send a variable to manually set it if you'd rather</param>
    public void ResetOpacity(float f = 1)
    {
        mat.SetFloat("_Opacity", f);
    }
}
