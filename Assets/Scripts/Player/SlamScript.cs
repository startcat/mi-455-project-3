using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlamScript : MonoBehaviour
{

    public float slamRadius = 10f;
    private float startRadius;
    private SphereCollider myCol;

    public float slamForce;
    public float slamTime = 1f;
    public float slamMaxSpeed = 50f;
    private float lifetime = 0f;

    [HideInInspector] public float impactSpeed;

    private List<Rigidbody> hitList = new List<Rigidbody>();

    // Start is called before the first frame update
    void Start()
    {
        myCol = GetComponent<SphereCollider>();
        startRadius = myCol.radius;
    }

    // Update is called once per frame
    void Update()
    {
        var scale = Mathf.Lerp(startRadius, slamRadius, lifetime / slamTime);
        myCol.radius = scale;

        lifetime += Time.deltaTime;
        if (lifetime >= slamTime) { Destroy(gameObject); }
    }

    void OnTriggerEnter(Collider col)
    {
        var bod = col.GetComponent<Rigidbody>();
        if (bod)
        {
            if(!hitList.Contains(bod)) 
            { 
                hitList.Add(bod);
                var dir = (bod.transform.position - transform.position).normalized;
                dir.y = 0.5f;
                var distScale = (slamRadius - Vector3.Distance(bod.transform.position, transform.position)) / slamRadius;
                bod.AddForce(dir * impactSpeed * distScale * slamForce);
                bod.velocity = Vector3.ClampMagnitude(bod.velocity, slamMaxSpeed);
                if (col.GetComponentInParent<ObjectHealth>()!= null)
                {
                    col.GetComponentInParent<ObjectHealth>().Damage(distScale * impactSpeed * slamForce);
                }
            }
        }
    }
}
