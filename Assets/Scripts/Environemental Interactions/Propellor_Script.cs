using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propellor_Script : MonoBehaviour
{
    Vector3 direction;

    public int forceValue;
    public ForceMode forceMode;
    // Start is called before the first frame update
    void Start()
    {
        direction = this.gameObject.transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(forceValue * direction, forceMode);
        }
    }
}
