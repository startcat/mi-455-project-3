using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extinguisher : MonoBehaviour
{
    [Range(0f, 10f)] public float activeTime = 5f;
    private float lifetime = 0f;

    [Range(0f, 5f)] public float randomTime = 0.5f;
    private float currentRandomTime;

    private Quaternion rotateFrom;
    private Quaternion rotateTarget;

    public float turnTime = 1f;
    private float currentTurnTime;

    public float activeSpeed = 10f;

    private Rigidbody myBod;

    private ParticleSystem smoke;
    // Start is called before the first frame update
    void Start()
    {
        myBod = GetComponent<Rigidbody>();
        smoke = GetComponentInChildren<ParticleSystem>();
        if (smoke) { var s = smoke.emission; s.enabled = false; }
    }

    // Update is called once per frame
    void Update()
    {
        if (lifetime > 0f) 
        { 
            lifetime -= Time.deltaTime; 
            myBod.velocity = transform.forward * activeSpeed;
            if (lifetime <= 0f) 
            {
                myBod.useGravity = true; 
                if (smoke) { var s = smoke.emission; s.enabled = false; } 
            }
            else 
            {
                if (currentRandomTime > 0f) 
                { 
                    currentRandomTime -= Time.deltaTime;
                    if (currentRandomTime <= 0f) 
                    { 
                        currentRandomTime = randomTime;
                        //rotateFrom = transform.eulerAngles;
                        //rotateTarget = Random.rotation.eulerAngles;
                        rotateFrom = Quaternion.LookRotation(transform.forward);
                        rotateTarget = Random.rotation;
                        currentTurnTime = turnTime;
                    }
                }

                if (currentTurnTime > 0f) { currentTurnTime -= Time.deltaTime; } 
                //else { currentTurnTime = 0f; }
                
                //transform.forward = Vector3.Lerp(rotateTarget, rotateFrom, currentTurnTime / turnTime);
                transform.rotation = Quaternion.Lerp(rotateTarget, rotateFrom, currentTurnTime / turnTime);
            }

        }
        
    }

    void OnTriggerEnter (Collider other)
    {
        if (lifetime <= 0f)
        {
            gameObject.GetComponentInChildren<Collider>().isTrigger = false;
            lifetime = activeTime;
            currentRandomTime = randomTime;
            currentTurnTime = turnTime;
            if (smoke) { var s = smoke.emission; s.enabled = true; }

            rotateFrom = Quaternion.LookRotation(transform.forward);
            rotateTarget = Quaternion.LookRotation(transform.forward);
        }
    }
}
